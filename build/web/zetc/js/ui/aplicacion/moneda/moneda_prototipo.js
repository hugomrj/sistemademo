

function Moneda(){
    
   this.tipo = "moneda";   
   this.recurso = "monedas";   
   this.value = 0;
   this.form_descrip = "moneda_nombre";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
   
   
   this.titulosin = "Moneda"
   this.tituloplu = "Monedas"   
      
   
   this.campoid=  'moneda';
   this.tablacampos =  ['moneda', 'nombre'];
   
    this.etiquetas =  ['Moneda', 'Nombre'];                                  
    
    
    this.tablaformat = ['C', 'C'];                                  
   
   this.tbody_id = "moneda-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "producto-acciones";   
         
   this.parent = null;
   
   

   
   
   
}





Moneda.prototype.lista_new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





Moneda.prototype.form_ini = function() {    

    
};



Moneda.prototype.form_validar = function() {    
    
   

    var moneda_nombre = document.getElementById('moneda_nombre');    
    if (moneda_nombre.value == "")         
    {
        msg.error.mostrar("Campo de nombre vacio");           
        moneda_nombre.focus();
        moneda_nombre.select();        
        return false;
    }       
        
    
    
    
    return true;
};







Moneda.prototype.main_list = function(obj, page) {    

        let promesa = cereza.vista.lista_paginacion(obj, page);
        
        promesa        
            .then(( xhr ) => {              
                cereza.html.url.redirect(xhr.status);                                                          
                // botones de accion - nuevo para este caso

            })
            .catch(( xhr ) => { 
                console.log(xhr.message);
            }); 

};



