/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.facturaventa;


import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


public class FacturaVentaDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public FacturaVentaDAO ( ) throws IOException  {
    }
      
    
    
    
    public List<FacturaVenta>  list (Integer page) {
                
        List<FacturaVenta>  lista = null;        
        try {                        
                        
            FacturaVentaRS rs = new FacturaVentaRS();            
            lista = new Coleccion<FacturaVenta>().resultsetToList(
                    new FacturaVenta(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
    
    
    
    
    public List<FacturaVenta>  search (Integer page, String busqueda) {
                
        List<FacturaVenta>  lista = null;
        
        try {                       
                        
            FacturaVentaRS rs = new FacturaVentaRS();
            lista = new Coleccion<FacturaVenta>().resultsetToList(
                    new FacturaVenta(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
              

    
    
    public FacturaVenta  update_monto (Integer codigo ) 
            throws IOException, SQLException, Exception {
            
        
        FacturaVenta  facturaventa = new FacturaVenta() ;        
        FacturaVentaRS rs = new FacturaVentaRS();        
        rs.update_monto(codigo);
        
        
        
        
        facturaventa = (FacturaVenta) persistencia.filtrarId(facturaventa, codigo);
        
        return facturaventa;

    }          
    
         
        
}
