/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.cliente;

import py.com.base.aplicacion.identificador_documento.IdentificadorDocumento;


/**
 *
 * @author hugo
 */


public class Cliente {
    
    private Integer cliente;
    private IdentificadorDocumento iden_doc;
    private String iden_numero;
    private Integer iden_digito;
    
    
    private String nombre;
    private String telefono;
    private String direccion;

    public Integer getCliente() {
        return cliente;
    }

    public void setCliente(Integer cliente) {
        this.cliente = cliente;
    }


    public String getIden_numero() {
        return iden_numero;
    }

    public void setIden_numero(String iden_numero) {
        this.iden_numero = iden_numero;
    }

    public Integer getIden_digito() {
        return iden_digito;
    }

    public void setIden_digito(Integer iden_digito) {
        this.iden_digito = iden_digito;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public IdentificadorDocumento getIden_doc() {
        return iden_doc;
    }

    public void setIden_doc(IdentificadorDocumento iden_doc) {
        this.iden_doc = iden_doc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
