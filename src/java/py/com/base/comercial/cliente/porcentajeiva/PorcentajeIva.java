/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.cliente.porcentajeiva;


/**
 *
 * @author hugo
 */
public class PorcentajeIva {
    
    private Integer porcentaje_iva;
    private String descripcion;

    public Integer getPorcentaje_iva() {
        return porcentaje_iva;
    }

    public void setPorcentaje_iva(Integer porcentaje_iva) {
        this.porcentaje_iva = porcentaje_iva;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}

