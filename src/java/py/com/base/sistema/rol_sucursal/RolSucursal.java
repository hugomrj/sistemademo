/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.sistema.rol_sucursal;


import py.com.base.sistema.rol.Rol;
import py.com.base.sistema.sucursal.Sucursal;



public class RolSucursal {
    
    
    private Integer id;
    private Rol rol = new Rol();
    private Sucursal sucursal = new Sucursal();

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    
}
