/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.preciocategoria;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class PrecioCategoriaDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public PrecioCategoriaDAO ( ) throws IOException  {
    }
      

    public List<PrecioCategoria>  all () {
                
        List<PrecioCategoria>  lista = null;        
        try {                        
                        
            PrecioCategoriaRS rs = new PrecioCategoriaRS();            
            lista = new Coleccion<PrecioCategoria>().resultsetToList(
                    new PrecioCategoria(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
    
    public List<PrecioCategoria>  list (Integer page) {
                
        List<PrecioCategoria>  lista = null;        
        try {                        
                        
            PrecioCategoriaRS rs = new PrecioCategoriaRS();            
            lista = new Coleccion<PrecioCategoria>().resultsetToList(
                    new PrecioCategoria(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<PrecioCategoria>  search (Integer page, String busqueda) {
                
        List<PrecioCategoria>  lista = null;
        
        try {                       
                        
            PrecioCategoriaRS rs = new PrecioCategoriaRS();
            lista = new Coleccion<PrecioCategoria>().resultsetToList(
                    new PrecioCategoria(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
        
    
    
}
