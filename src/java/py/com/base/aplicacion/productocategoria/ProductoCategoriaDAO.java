/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.productocategoria;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class ProductoCategoriaDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public ProductoCategoriaDAO ( ) throws IOException  {
    }
      

    public List<ProductoCategoria>  all () {
                
        List<ProductoCategoria>  lista = null;        
        try {                        
                        
            ProductoCategoriaRS rs = new ProductoCategoriaRS();            
            lista = new Coleccion<ProductoCategoria>().resultsetToList(
                    new ProductoCategoria(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
    
    public List<ProductoCategoria>  list (Integer page) {
                
        List<ProductoCategoria>  lista = null;        
        try {                        
                        
            ProductoCategoriaRS rs = new ProductoCategoriaRS();            
            lista = new Coleccion<ProductoCategoria>().resultsetToList(
                    new ProductoCategoria(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<ProductoCategoria>  search (Integer page, String busqueda) {
                
        List<ProductoCategoria>  lista = null;
        
        try {                       
                        
            ProductoCategoriaRS rs = new ProductoCategoriaRS();
            lista = new Coleccion<ProductoCategoria>().resultsetToList(
                    new ProductoCategoria(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
        
    
    
}
