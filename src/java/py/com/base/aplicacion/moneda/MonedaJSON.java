/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.moneda;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Map;
import nebuleuse.ORM.JsonObjeto;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.RegistroMap;
import nebuleuse.ORM.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class MonedaJSON  {


    
    
    public MonedaJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  lista ( Integer page) {
        
        
        JsonObject jsonObject = new JsonObject();
        
        try 
        {   
            
            ResultadoSet resSet = new ResultadoSet();                   
            String sql = SentenciaSQL.select(new Moneda());       
            
            ResultSet rsData = resSet.resultset(sql, page);                
            
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     

            //PresupuestoConsultaRS objetoRS_suma  = new PresupuestoConsultaRS();
    
            
            /*
            ResultSet rsSuma = rs.tabdata_sum(
                n1, n2, n3, obj, ff, of, dpto );                
            
            JsonArray jsonarraySuma = new JsonArray();
            while(rsSuma.next()) 
            {  
                
                map = registoMap.convertirHashMap(rsSuma);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarraySuma.add( element );

            }                    
            */

            

    


        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
    
    
        
}
