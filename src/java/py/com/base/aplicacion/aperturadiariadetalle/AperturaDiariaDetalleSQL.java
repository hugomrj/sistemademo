/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.aperturadiariadetalle;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class AperturaDiariaDetalleSQL {
    
    

    
    public String lista ( Integer apertura ) 
            throws Exception {

    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("AperturaDiariaDetalle");
        reader.fileExt = "lista.sql";
        
        sql = reader.get(apertura );    
        
        return sql ;             
        
        
    }        
       
    
    
    public String listan_data ( Integer apertura ) 
            throws Exception {

    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("AperturaDiariaDetalle");
        reader.fileExt = "listan_data.sql";
        
        sql = reader.get(apertura );    
        
        return sql ;             
        
        
    }        
       
    
    
    
    public String listan_sum ( Integer apertura ) 
            throws Exception {

    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("AperturaDiariaDetalle");
        reader.fileExt = "listan_sum.sql";
        
        sql = reader.get(apertura );    
        
        return sql ;             
        
        
    }        
       
    
    
    
    
    
}
