/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.aperturadiariadetalle;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


/**
 *
 * @author hugom_000
 */

public class AperturaDiariaDetalleDAO  {
        
    /*
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;  
        Lista lista ;
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
    */ 
        
    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    private Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    
        
    
    public AperturaDiariaDetalleDAO ( ) throws IOException  {

    }
       
       


    
    public void agregarlotefecha (Integer apertura) throws Exception {      

          String sql = 
            " INSERT INTO aplicacion.apertura_diaria_detalle\n" +
            " (producto, precio_venta, apertura_cantidad, cierre_cantidad, venta_cantidad, monto_ingreso, apertura)\n" +
            " SELECT producto, precio_venta, 0, null, null, null, "
                  + apertura  + "\n" +
            " FROM aplicacion.productos ";
          
          Persistencia persistencia = new Persistencia();
          
          persistencia.ejecutarSQL(sql);
          
      }
     

       
    
    public List<AperturaDiariaDetalle>  lista (Integer apertura) {
                
        List<AperturaDiariaDetalle>  lista = null;        
        try {                        
                        
            AperturaDiariaDetalleRS rs = new AperturaDiariaDetalleRS();            
            
            
            lista = new Coleccion<AperturaDiariaDetalle>().resultsetToList(
                    new AperturaDiariaDetalle(),
                    rs.lista( apertura)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    
    
    public void actualizarRegistro(Integer id, String json ) {      

        
        try {
        
            
            Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
            
            AperturaDiariaDetalle aperturadiariadetalle = new AperturaDiariaDetalle();
            JsonObject jsonobject = new Gson().fromJson(json, JsonObject.class);
            
            String sql = "";
  
            JsonElement typocierre = jsonobject.get("cierre_cantidad");            
            
            if (typocierre.getClass().getName().trim().equals("com.google.gson.JsonNull")){

                
                Integer apertura_cantidad = jsonobject.get("apertura_cantidad").getAsInt()   ;                
                
                sql = "UPDATE aplicacion.apertura_diaria_detalle\n" +
                                    "SET \n" +
                                    "	apertura_cantidad= " + apertura_cantidad +", " +
                                    "	cierre_cantidad=null,\n" +
                                    "	venta_cantidad = null,\n" +
                                    "	monto_ingreso = null	\n" +
                                    "WHERE id=" + id ;
                
            }
            else{
            
                Integer apertura_cantidad = jsonobject.get("apertura_cantidad").getAsInt()   ;
                Integer cierre_cantidad = jsonobject.get("cierre_cantidad").getAsInt()   ;
                Integer venta_cantidad = apertura_cantidad - cierre_cantidad;
                
                
                sql = "UPDATE aplicacion.apertura_diaria_detalle\n" +
                                    "SET \n" +
                                    "	apertura_cantidad= " + apertura_cantidad +", " +
                                    "	cierre_cantidad=" + cierre_cantidad +", " +
                                    "	venta_cantidad = " + venta_cantidad +", " +
                                    "	monto_ingreso = precio_venta  * " + venta_cantidad  +
                                    "WHERE id=" + id ;
                
                
            }
            
            
            Persistencia persistencia = new Persistencia();
          
            persistencia.ejecutarSQL(sql);
    
            
            
            /*
            
            JsonObject jsonobject = new Gson().fromJson(json, JsonObject.class);
            Integer apertura_cantidad = jsonobject.get("apertura_cantidad").getAsInt()   ;


            

            
                       
            
            JsonElement typocierre = jsonobject.get("cierre_cantidad");
            
            System.out.println(typocierre.getClass().getName());
            
            
            if (typocierre.getClass().getName().trim().equals("com.google.gson.JsonNull")){
                
                
                System.out.println("es nulo");
                
                
            }
            else{

            aperturadiariadetalle.setApertura_cantidad(apertura_cantidad);                
                
                Integer cierre_cantidad = jsonobject.get("cierre_cantidad").getAsInt()   ;
            
                aperturadiariadetalle.setCierre_cantidad(cierre_cantidad);
                aperturadiariadetalle.setVenta_cantidad( apertura_cantidad - cierre_cantidad  );
                aperturadiariadetalle.setMonto_ingreso(
                        aperturadiariadetalle.getPrecio_venta() * aperturadiariadetalle.getVenta_cantidad()
                );
                
                
                
            }
            
            
            
            
            */
            
            
            
            
            
            

        } catch (Exception ex) {
            Logger.getLogger(AperturaDiariaDetalleDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getCause());
        }
        
    }
     

    
    
    
      
    
        
}
